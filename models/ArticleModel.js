const mongoose  = require('mongoose');

let ArticleSchema = mongoose.Schema(
    {
        userId: {
            type: String,
            required: false
        },
        name: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        firstCategorie: {
            type: String,
            required: false
        },
        secondCategorie: {
            type: String,
            required: false
        },
        categorieId: {
            type: String,
            required: true
        },
        marqueId: {
            type: String,
            required: true
        },
        etat: {
            type: String,
            required: false
        },
        price: {
            type: Number,
            required: true
        },
        pictures:{
            type: [String],
            required: false
        },
        available:{
            type: Boolean,
            default: true,
            required: false
        }
    },
);

ArticleSchema.methods.toJSON = function () {
    const obj = this.toObject();
    obj.articleId = this._id;
    delete obj.__v;
    return obj;
};

const Article = mongoose.model('Article', ArticleSchema);
module.exports = Article;

// default values which displayed is 10
module.exports.getAllArticles = (limitNbre='5',skip='0')=>{
  return Article.find()
      .sort({_id: -1})
      .limit(parseInt(limitNbre))
      .skip(parseInt(skip))
      .exec();
};

module.exports.getArticleByCategorieId = (categorieId,limitNbre='10',skip='0')=>{
    const query ={categorieId: categorieId};
    return Article.find(query)
        .sort({_id: -1})
        .limit(parseInt(limitNbre))
        .skip(parseInt(skip))
        .exec();
};

module.exports.getArticleByFiltre = (categorieId,firstCategorie,secondCategorie,marqueId,etat,userId,min,max)=>{
    console.log("Min= "+min+" Max = "+max);
    // if (min === undefined || min.trim() === "" || min.trim()) {min = 0}
    // if (max === undefined || max.trim() === "" || max.trim()) {max = 99999}
    console.log("Min= "+min+" Max = "+max);
    const query = {$or:[
            {categorieId: categorieId},
            {firstCategorie: firstCategorie},
            {secondCategorie: secondCategorie},
            {marqueId: marqueId},
            {etat: etat},
            {userId: userId},
            {price: {$gte:min, $lt:max}}
        ]};
    return Article.find(query).exec();
};