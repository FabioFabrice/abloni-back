const mongoose  = require('mongoose');
const bcrypt    = require('bcryptjs');

const UserModelSchema = mongoose.Schema(
    {
        picture: {
            type: String,
            required: false
        },
        lastname: {
            type: String,
            required: true
        },
        firstname: {
            type: String,
            required: true
        },
        phoneNumber: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        lastJwtToken: {
            type: String,
            required: false
        },
        favorites: {
            type: [String],
            required: false,
        },
        country: {
            type: String,
            required: false
        }
    },
);

UserModelSchema.methods.toJSON = function() {
    const obj = this.toObject();
    delete obj.password;
    delete obj.__v;
    obj.userId = this._id;
    return obj;
};

const User = mongoose.model('User', UserModelSchema);
module.exports = User;

// Get User by phoneNumber
module.exports.getUserByPhoneNumber = (_phoneNumber) => {
    console.log("step 2 : "+_phoneNumber);
    const query = {phoneNumber: _phoneNumber};
    const x =  User.findOne(query).exec();
    console.log("x : "+JSON.stringify(x,null,4));
    return x;
};

// Get User by id
module.exports.getUserById = (id) => {
    const query = {_id: id};
    return User.findOne(query).exec();
};

// Get User by pseudo
module.exports.getUserPseudo = (_pseudo) => {
    const query = {pseudo: _pseudo};
    return User.findOne(query).exec();
};

// Compare passpord and its hash
module.exports.comparePassword = function(userPassword, hash, callback){
    bcrypt.compare(userPassword, hash, function(err, isMatch) {
        if(err) throw err;
        callback(null, isMatch);
    });
};

// Set user's token
module.exports.setLastToken = (phoneNumber, lastJwtToken) => {
    const query = {phoneNumber: phoneNumber};
    const newvalues = { $set: {lastJwtToken: lastJwtToken } };
    return User.updateOne(query, newvalues).exec();
};

// update user's password
module.exports.updatePwd = (phoneNumber, pwd) => {
    const query = {phoneNumber: phoneNumber};
    const newvalues = { $set: {password: pwd } };
    return User.updateOne(query, newvalues).exec();
};

// update user's infos
module.exports.updateProfil = (phoneNumber, lastname, firstname, picture) => {
    const query = {phoneNumber: phoneNumber};
    const newvalues = { $set: {lastname: lastname, firstname:firstname, picture:picture} };
    return User.updateOne(query, newvalues).exec();
}
