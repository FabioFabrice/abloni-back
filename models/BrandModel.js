const mongoose  = require('mongoose');

let BrandSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true
        },
        parentId: {
            type: String,
            default: "0",
            required: true
        },
        haveChild:{
            type: Number,
            default: 0,
            required: false
        }
    },
);

BrandSchema.methods.toJSON = function () {
    const obj = this.toObject();
    obj.brandId = this._id;
    delete obj.__v;
    return obj;
};

const Brand = mongoose.model('Brand', BrandSchema);
module.exports = Brand;

module.exports.getAllBrand = () => {
  return Brand.find()
      .sort({haveChild: -1})
      .exec();
};

module.exports.increaseParentValues = (parentId) =>{
    console.log("inc : "+parentId);
    const query = {_id: parentId};
    const newvalues = { $inc: {haveChild: 1}};
    return Brand.updateOne(query,newvalues).exec();
};

module.exports.decreaseParentValues = (parentId) =>{
    console.log("desc : "+parentId);
    const query = {_id: parentId};
    const newvalues = { $inc: {haveChild: -1}};
    return Brand.updateOne(query,newvalues).exec();
};

module.exports.getBrand = (parentId) => {
    const query = {parentId: parentId};
    return Brand.find(query)
        .sort({haveChild: -1})
        .exec();
};