const mongoose  = require('mongoose');

let CategorieSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true
        },
        parentId: {
            type: String,
            default: "0",
            required: false
        },
        haveChild:{
            type: Number,
            default: 0,
            required: false
        }
    },
);



CategorieSchema.methods.toJSON = function () {
    const obj = this.toObject();
    obj.categorieId = this._id;
    delete obj.__v;
    return obj;
};

const Categorie = mongoose.model('Categorie', CategorieSchema);
module.exports = Categorie;

module.exports.getAllCategories = () => {
    return Categorie.find()
        .sort({haveChild: -1})
        .exec();
};

module.exports.increaseParentValues = (parentId) =>{
    console.log("inc : "+parentId);
    const query = {_id: parentId};
    const newvalues = { $inc: {haveChild: 1}};
    return Categorie.updateOne(query,newvalues).exec();
};

module.exports.decreaseParentValues = (parentId) =>{
    console.log("desc : "+parentId);
    const query = {_id: parentId};
    const newvalues = { $inc: {haveChild: -1}};
    return Categorie.updateOne(query,newvalues).exec();
};

module.exports.getCategories = (parentId) => {
    const query = {parentId: parentId};
    return Categorie.find(query)
        .sort({haveChild: -1})
        .exec();
};
