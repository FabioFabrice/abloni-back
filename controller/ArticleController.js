const Utils = require('../config/Utils');
const ArticleModel = require('./../models/ArticleModel');

require('dotenv').config();

module.exports = {
    // add new article
    add_new_article:(req,res)=>{
        const article = new ArticleModel();
        Utils.verifyToken(req,res,(err,tokenInfo)=>{
            if(err){
                return Utils.getJsonResponse(500,'Error token', {}, res);
            }
            else{
                const {
                    userId,name,description,
                    firstCategorie,secondCategorie,
                    categorieId,marqueId,
                    etat,price,pictures
                } = req.body;
                article.userId = userId;
                article.name = name;
                article.description = description;
                article.firstCategorie = firstCategorie;
                article.secondCategorie = secondCategorie;
                article.categorieId = categorieId;
                article.marqueId = marqueId;
                article.etat = etat;
                article.price = price;
                article.pictures = pictures;
                // const data = {
                //     id: article._id,
                //     name: article.name,
                //     pictures: article.pictures,
                //     avaible: article.avaible,
                //     message: 'New article has been added'
                // };
                article.save()
                    .then(art => {
                        console.log("New article has been added");
                        return Utils.getJsonResponse(200,'',{},res)
                    })
                    .catch(_err=>{
                        console.log("error add aricle : "+_err);
                        return Utils.getJsonResponse(500,'Article has not been saved',{},res)
                    })
            }
        })
    },

    // make article unavailable
    unavailable_article: (req,res) => {
        Utils.verifyToken(req,res,(err,tokenInfo)=>{
            if(err){
                return Utils.getJsonResponse(500,'Error token', {}, res);
            }else{
                const {userId,articleId} = req.body;
                ArticleModel.findOneAndUpdate({userId: userId, articleId: articleId},{available: false}, err=>{
                    if (err) return Utils.getJsonResponse(500,'Article has not been unavailable',{},res);
                    else return Utils.getJsonResponse(200,'Article has been unavailable',{},res)
                })
            }
        })
    },

    // get all article
    get_all_article: (req,res)=>{
        const {limitNbre,skip} = req.query;
        ArticleModel.getAllArticles(limitNbre,skip)
            .then(art => {
                console.log("art : " + art);
                return Utils.getJsonResponse(200, '', art, res);
            })
            .catch(err => {
                return Utils.getJsonResponse(403, 'Articles not found', {}, res)
            })
    },

    // get article by id
    get_article_by_categorie: (req,res)=>{
        const {categorieId,limitNbre,skip} = req.query;
        ArticleModel.getArticleByCategorieId(categorieId,limitNbre,skip)
            .then(art=>{
                return Utils.getJsonResponse(200,'',art,res);
            })
            .catch(err=>{
                return  Utils.getJsonResponse(403,'Articles not found',{}, res)
            })
    },

    // get article by all filtre
    get_article_by_filre: (req,res)=>{
        const {
            categorieId,firstCategorie,
            secondCategorie,marqueId,
            etat,userPseudo,priceMin,priceMax
        } = req.query;

        ArticleModel.getArticleByFiltre(categorieId,firstCategorie,secondCategorie,marqueId,etat,userPseudo,priceMin,priceMax)
            .then(art=>{
                return Utils.getJsonResponse(200,'',art,res);
            })
            .catch(err=>{
                return  Utils.getJsonResponse(403,'Articles not found',{}, res)
            })
    },



};