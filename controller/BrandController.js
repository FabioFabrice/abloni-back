const Utils = require('../config/Utils');
const BrandModel = require('../models/BrandModel');

require('dotenv').config();

module.exports = {
    // add new brand
    add_new_brand: (req,res)=>{
        Utils.verifyToken(req,res,(err,tokenInfo)=>{
            if(err){
                return Utils.getJsonResponse(500,'Error token', {}, res);
            }else{
                const brand = new BrandModel();
                const {name,parentId} = req.body;
                if (parentId !== undefined){
                    if(!parentId.trim().isEmpty){
                        brand.parentId= parentId;
                    }
                }
                brand.name = name;

                if(parentId !== "0"){
                    BrandModel.increaseParentValues(parentId)
                        .then(result=>{
                            brand.save()
                                .then(cat => {
                                    console.log("New brand has been add");
                                    return Utils.getJsonResponse(200,'',brand,res)
                                })
                                .catch(_err=>{
                                    return Utils.getJsonResponse(500,'Brand has not been saved',{},res)
                                })
                        })
                        .catch(err=>{
                            return Utils.getJsonResponse(401,'ParentID column has not been inc', {},res)
                        })
                }else{
                    brand.save()
                        .then(cat => {
                            console.log("New brand has been add");
                            return Utils.getJsonResponse(200,'',brand,res)
                        })
                        .catch(_err=>{
                            return Utils.getJsonResponse(500,'Brand has not been saved',{},res)
                        })
                }
            }
        })
    },

    // delete brand
    delete_brand: (req,res)=>{
        Utils.verifyToken(req,res,(err,tokenInfo)=>{
            if(err){
                return Utils.getJsonResponse(500,'Error token', {}, res);
            }else{
                const data = {
                    message: 'Brand has been deleted'
                };
                const {id, parentId} = req.body;

                if(parentId !== "0"){
                    BrandModel.decreaseParentValues(parentId)
                        .then(result=>{
                            BrandModel.findOneAndDelete({_id: req.body.id}, err=>{
                                if (err) return Utils.getJsonResponse(500,'Brand has not been deleted',{},res);
                                else return Utils.getJsonResponse(200,'',data,res)
                            })
                        })
                        .catch(err=>{
                            return Utils.getJsonResponse(401,'ParentID column has not been desc', {},res)
                        })
                }else{
                    BrandModel.findOneAndDelete({_id: req.body.id}, err=>{
                        if (err) return Utils.getJsonResponse(500,'Brand has not been deleted',{},res);
                        else return Utils.getJsonResponse(200,'',data,res)
                    })
                }
            }
        })
    },

    // get all Brand
    get_all_brand: (req,res)=>{
        BrandModel.getAllBrand()
            .then(cat => {
                return Utils.getJsonResponse(200,'',cat,res);
            })
            .catch( _err => {
                return Utils.getJsonResponse(403,'Brand not found',{}, res)
            })
    },

    // get principal Brand
    get_principal_brand: (req,res)=>{
        BrandModel.getBrand('0')
            .then( brd =>{
                return Utils.getJsonResponse(200,'',brd,res)
            })
            .catch( _err => {
                return Utils.getJsonResponse(403,'Brand not found',{}, res)
            })
    },

    // get sub brand
    get_sub_brand: (req,res)=>{
        const parentId = req.query.parentId;
        if(parentId !== undefined){
            if(!parentId.trim().isEmpty){
                BrandModel.getBrand(parentId)
                    .then( brd => {
                        return Utils.getJsonResponse(200,'',brd,res)
                    }).catch( _err => {
                    return Utils.getJsonResponse(403,'Brand not found',{}, res)
                })
            }
        }
        else{
            Utils.getJsonResponse(403,'ParentId can not be empty',{},res)
        }
    }
};