const jwt                   = require('jsonwebtoken');
const bcrypt                = require('bcryptjs');
const UserModel             = require('./../models/UserModel');
const Utils                 = require('../config/Utils');
const passport = require("passport");

require('dotenv').config();


const { body, check }   = require('express-validator/check');

module.exports = {

    // resgiter
    register: async (req, res) => {
        req
            .getValidationResult()
            .then( (err, result) => {
                if (!err.isEmpty()) {
                    return  Utils.getErrors(res, err);
                } else {
                    const { lastname, firstname, phoneNumber, password, country} = req.body;

                    console.log("lastname : "+lastname+" firstname : "+firstname+" phoneNumber : "+phoneNumber+" password : "+password);
                    // Check phone number
                    UserModel.getUserByPhoneNumber(phoneNumber).then( user => {
                        // if user with this phoneNumber exists return 403
                        if (user) {
                            // user already exist
                            return Utils.getJsonResponse(403, 'User already exist', {}, res);
                        } else {
                            const userInstance = new UserModel();
                            bcrypt.genSalt(10, (err, salt) => {
                                bcrypt.hash(password, salt, async (err, hash) => {
                                    if (err) {
                                        return Utils.getJsonResponse(404, 'Error hash pwd', {}, res);
                                    } else {
                                        userInstance.lastname = lastname;
                                        userInstance.firstname = firstname;
                                        userInstance.phoneNumber = phoneNumber;
                                        userInstance.password = hash;
                                        userInstance.country = country;
                                        userInstance.save()
                                            .then( result => {
                                                return Utils.getJsonResponse(200, '', userInstance, res);
                                            })
                                            .catch(e => {
                                                console.log('User not save ', e);
                                                return Utils.getJsonResponse(500, e, {}, res);
                                            });
                                    }
                                })
                            });
                        }
                    });
                }
            });
    },

    // login
    login:  async (req, res) => {
        req
            .getValidationResult()
            .then( (err, result) => {
                if (!err.isEmpty()) {
                    return Utils.getErrors(res, err);
                } else {
                    console.log("step 1 ");
                    passport.authenticate('local',
                        {session:false},
                        (err,user,info)=>{
                        if(err){
                            return Utils.getJsonResponse(500,err, {}, res);
                        }
                        if(info){
                            return Utils.getJsonResponse(info.statusCode,info.message, {}, res);
                        }
                        else{
                            return Utils.getJsonResponse(200,'', user, res);
                        }
                    })(req,res);
                }

            });
    },

    // recover account 1
    recover_account_1: async (req,res) => {
        const {phoneNumber} = req.body;

        UserModel.getUserByPhoneNumber(phoneNumber).then( user => {
            if (user) {
                return Utils.getJsonResponse(200,'', {}, res);
            } else {
                return Utils.getJsonResponse(404,'Phone number not exits', {}, res);
            }
        })
    },

    // recover account 1
    recover_account_2: async (req,res) => {
        req
            .getValidationResult()
            .then((err,result)=>{
                if(!err.isEmpty()){
                    return  Utils.getErrors(res, err);
                }else{
                    const {phoneNumber, password} = req.body;

                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(password, salt, async (err, hash) => {
                            if (err) {
                                return Utils.getJsonResponse(404, 'Error hash pwd', {}, res);
                            } else {
                                UserModel.updatePwd(phoneNumber,hash)
                                    .then( result => {
                                        return Utils.getJsonResponse(200,'', {}, res);})
                                    .catch( err => {
                                        // console.log('False --------------> '+user.phoneNumber);
                                        return Utils.getJsonResponse(404,'Can not update user password', {}, res);})
                            }
                        })
                    });
                }
            })
    },

    // update user settings
    update_profil: async (req,res)=>{
        Utils.verifyToken(req,res,(err,tokenInfo)=>{
            if(err){
                return Utils.getJsonResponse(500,'Error token', {}, res);
            }else{
                const {phoneNumber, lastname, firstname, picture} = req.body;
                const data = {
                    message: "User profil settings"
                }
                if(phoneNumber !== undefined && lastname !== undefined
                    && firstname !== undefined && picture !== undefined){
                    if(!phoneNumber.trim().isEmpty && !lastname.trim().isEmpty
                        && !firstname.trim().isEmpty && !picture.trim().isEmpty){
                        UserModel.updateProfil(phoneNumber,lastname,firstname,picture)
                            .then(usr=>{
                                return Utils.getJsonResponse(200,'', data, res);
                            }).catch( err => {
                            // console.log('False --------------> '+user.phoneNumber);
                            return Utils.getJsonResponse(404,'Can not update user profil', {}, res);
                        })
                    }else{
                        return Utils.getJsonResponse(404,'Some fields can not be empty', {}, res);
                    }
                }else{
                    return Utils.getJsonResponse(404,'Some fields required', {}, res);
                }
            }
        })
    },

    // get all info from user
    user_infos: async (req,res)=>{
        Utils.verifyToken(req,res,(err,tokenInfo)=>{
            if(err){
                return Utils.getJsonResponse(500,'Error token', {}, res);
            }else{
                const phoneNumber = req.query.phoneNumber;
                // nodejs can't see '+' so it replace it by escape
                const _phoneNumber = ("+"+phoneNumber.trim().replace(" ",""))
                    .replace(" ","");
                UserModel.getUserByPhoneNumber(_phoneNumber)
                    .then(data=>{
                        console.log("data : "+data);
                        return Utils.getJsonResponse(200,'', data, res);
                    }).catch( err => {
                    // console.log('False --------------> '+user.phoneNumber);
                    return Utils.getJsonResponse(404,'Can not update user profil', {}, res);
                })
            }
        })
    },
};


module.exports.validate = (method) => {

    switch (method) {
        case 'login': {
            return [
                check('phoneNumber', "Param phoneNumber doesn't exist").trim().exists(),
                check('phoneNumber', "Please enter your phoneNumber").not().isEmpty(),
                check('phoneNumber', "This phoneNumber not found").trim().custom(value =>  {
                    return new Promise((resolve, reject) => {
                        UserModel.getUserByPhoneNumber(value).then( user => {
                            if (user) {
                                return resolve();
                            } else {
                                return reject();
                            }
                        })
                    })
                }),
                check('password', "password's parameter password doesn't exist").trim().exists(),
                check('password', "Please enter password").not().isEmpty(),
                check('password', "Password must contain at least number and must have a length of 6").matches('^(?=.*?[a-z])(?=.*?[0-9]).{6,}$'),
            ]
        }
        case 'register': {
            return [
                check('phoneNumber', "phoneNumber's parameter doesn't exist").trim().exists(),
                check('phoneNumber', "Please enter your phoneNumber").trim().not().isEmpty(),
                check('phoneNumber', "This phoneNumber is already used").trim().custom(value =>  {
                    return new Promise((resolve, reject) => {
                        UserModel.getUserByPhoneNumber(value).then( user => {
                            if (user) {
                                return reject();
                            } else {
                                return resolve() ;
                            }
                        })
                    })
                }),
                check('password', "password's parameter password doesn't exist").trim().exists(),
                check('password', "Please enter password").not().isEmpty(),
                check('password', "Password must contain at least number and must have a length of 6").matches('^(?=.*?[a-z])(?=.*?[0-9]).{6,}$'),
            ]
        }
        default : {

        }
    }

};

