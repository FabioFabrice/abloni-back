const Utils = require('../config/Utils');
const CategorieModel = require('./../models/CategorieModel');

require('dotenv').config();

module.exports = {
    // add new categorie
    add_new_categorie: (req,res) => {
        Utils.verifyToken(req,res,(err,tokenInfo)=>{
            if(err){
                return Utils.getJsonResponse(500,'Error token', {}, res);
            }else{
                const categorie = new CategorieModel();
                const { name, parentId } = req.body;
                if (parentId !== undefined){
                    if(!parentId.trim().isEmpty){
                        categorie.parentId = parentId;
                    }
                }
                categorie.name = name;

                // The have not categorie with id == 0
                if(parentId !== "0"){
                    console.log("P1 : "+parentId);
                    CategorieModel.increaseParentValues(parentId)
                        .then(result=>{
                            console.log("result : "+JSON.stringify(result,null,4));
                            categorie.save()
                                .then(cat => {
                                    console.log("New cat has been add");
                                    return Utils.getJsonResponse(200,'',categorie ,res)
                                })
                                .catch(_err => {
                                    return Utils.getJsonResponse(500,'Categorie has not been saved', {},res)
                                });
                        })
                        .catch(err=>{
                            return Utils.getJsonResponse(401,'ParentID column has not been inc', {},res)
                        })
                }else{
                    console.log("P2");
                    categorie.save()
                        .then(cat => {
                            console.log("New cat has been add");
                            return Utils.getJsonResponse(200,'',categorie ,res)
                        })
                        .catch(_err => {
                            return Utils.getJsonResponse(500,'Categorie has not been saved', {},res)
                        });
                }
            }
        })
    },

    // delete categorie
    delete_categorie: (req,res) => {
        Utils.verifyToken(req,res,(err,tokenInfo)=>{
            if(err){
                return Utils.getJsonResponse(500,'Error token', {}, res);
            }else{
                const data = {
                    message: 'Categorie has been deleted'
                };
                const { id, parentId } = req.body;
                console.log("id : "+id);

                if(parentId !== "0"){
                    CategorieModel.decreaseParentValues(parentId)
                        .then(result=>{
                            console.log("result : "+JSON.stringify(result,null,4));
                            CategorieModel.findOneAndDelete({_id: id}, {},(err,doc) => {
                                if (err) return Utils.getJsonResponse(500,'Categorie has not been deleted',{},res);
                                else return Utils.getJsonResponse(200,'',data,res)
                            })
                        })
                        .catch(err=>{
                            return Utils.getJsonResponse(401,'ParentID column has not been desc', {},res)
                        })
                }else{
                    CategorieModel.findOneAndDelete({_id: id}, {},(err,doc) => {
                        if (err) return Utils.getJsonResponse(500,'Categorie has not been deleted',{},res);
                        else return Utils.getJsonResponse(200,'',data,res)
                    })
                }
            }
        })
    },


    // get all categorie
    get_all_categorie: (req,res) => {
        // const categorie = new CategorieModel();
        CategorieModel.getAllCategories()
            .then(cat => {
                return Utils.getJsonResponse(200,'',cat,res);
            })
            .catch( _err => {
                return Utils.getJsonResponse(403,_err,'Categorie not found', res)
            })
    },

    // get principal Categorie
    get_principal_categorie: (req,res) => {
        CategorieModel.getCategories('0')
            .then( cat => {
                return Utils.getJsonResponse(200,'',cat,res)
            })
            .catch( _err => {
            return Utils.getJsonResponse(403,'Categorie not found',{}, res)
        })
    },

    // get sub categorie
    get_sub_categorie: (req,res) => {
        const parentId = req.query.parentId;
        if (parentId !== undefined){
            if(!parentId.trim().isEmpty){
                CategorieModel.getCategories(parentId)
                    .then( cat => {
                        return Utils.getJsonResponse(200,'',cat,res)
                    }).catch( _err => {
                    return Utils.getJsonResponse(403,'Categorie not found',{}, res)
                })
            }
        }
        else {
            Utils.getJsonResponse(403,'ParentId can not be empty',{},res)
        }
    }
};