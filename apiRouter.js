const express = require('express');
const UserController = require('./controller/UserController');
const CategorieController = require("./controller/CategorieController");
const BrandController = require("./controller/BrandController");
const ArticleController = require("./controller/ArticleController");

exports.router = (function() {
    const apiRouter = express.Router();

    // login - register - recover user's account
    apiRouter.route('/register').post(UserController.validate('register'), UserController.register);
    apiRouter.route('/login').post(UserController.validate('login'), UserController.login);
    apiRouter.route('/recover/account/1').post(UserController.recover_account_1);
    apiRouter.route('/recover/account/2').post(UserController.validate('login'), UserController.recover_account_2);

    // update user profil
    apiRouter.route('/users/profil').post(UserController.update_profil);
    apiRouter.route('/users/infos').get(UserController.user_infos);

    // manage categories
    apiRouter.route('/categorie/add').post(CategorieController.add_new_categorie);
    apiRouter.route('/categorie/delete').post(CategorieController.delete_categorie);
    apiRouter.route('/categorie/all').get(CategorieController.get_all_categorie);
    apiRouter.route('/categorie/principal').get(CategorieController.get_principal_categorie);
    apiRouter.route('/categorie/children').get(CategorieController.get_sub_categorie);

    // manage brands
    apiRouter.route('/brand/add').post(BrandController.add_new_brand);
    apiRouter.route('/brand/delete').post(BrandController.delete_brand);
    apiRouter.route('/brand/all').get(BrandController.get_all_brand);
    apiRouter.route('/brand/principal').get(BrandController.get_principal_brand);
    apiRouter.route('/brand/children').get(BrandController.get_sub_brand);

    apiRouter.route('/article/add').post(ArticleController.add_new_article);
    apiRouter.route('/article/unavailable').post(ArticleController.unavailable_article);
    apiRouter.route('/article/all').get(ArticleController.get_all_article);
    apiRouter.route('/article/filtre/categorie').get(ArticleController.get_article_by_categorie);
    apiRouter.route('/article/filtre/all').get(ArticleController.get_article_by_filre);

    return apiRouter;
})();
