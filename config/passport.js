const UserModel = require("../models/UserModel");
const config = require("./config");
const randomstring = require("randomstring");
const jwt = require("jsonwebtoken");
const LocalStrategy     = require('passport-local').Strategy;

module.exports = function (passport) {
    passport.use(
        new LocalStrategy({
            usernameField: 'phoneNumber',
            passwordField: 'password'
        },
        (phoneNumber, password, done)=>{
            console.log(phoneNumber+" -> "+password);
            UserModel.getUserByPhoneNumber(phoneNumber)
                .then( user => {
                    if(user) {
                        UserModel.comparePassword(password, user.password, function(err, isMatch){
                            if (err) { return done(err); }
                            if (isMatch) {
                                const token = jwt.sign(
                                    { birthDate: user._id, phoneNumber: randomstring.generate({ length: 26, charset: 'alphanumeric'}) },
                                    config.secret);

                                const data = {
                                    lastname: user.lastname,
                                    firstname: user.firstname,
                                    phoneNumber: user.phoneNumber,
                                    picture: user.picture,
                                    favorites: user.favorites,
                                    country: user.country,
                                    userId: user._id,
                                    token: token,
                                };

                                UserModel.setLastToken(user.phoneNumber,token)
                                    .then( result => {
                                        // console.log('True --------------> '+user.phoneNumber);
                                        return done(null, data);
                                    })
                                    .catch( err => {
                                        // console.log('False --------------> '+user.phoneNumber);
                                        return done(null, false, {statusCode : 404, message : 'Can not set user token'});
                                    })
                            } else {
                                // Invalid password
                                return done(null, false, {statusCode : 401, message : 'Wrong Password'});
                            }
                        });
                    }
                    else {
                        // This phone number is not found
                        return done(null, false, {statusCode : 404, message : 'Phone number not exits'});
                    }
                })
                .catch( err => {
                    return done(null, false, {statusCode : 500, message : 'Internal error'});
                });
        })
    );

    // passport.serializeUser((user, done) => {
    //     console.log("?1");
    //     done(null, user.id);
    // });
    //
    // passport.deserializeUser((id, done) => {
    //     console.log("?2");
    //     UserModel.findById(id, (err, user) => {
    //         done(err, user);
    //     });
    // });
}