
const express                   = require('express');
const apiRouter                 = require('./apiRouter').router;
const expressValidator          = require('express-validator');
const bodyParser                = require('body-parser');
// const Passport                  = require('./config/passportJWT');
const passport                  = require('passport');
const port = process.env.PORT || 3000;
const mongoose  = require('mongoose');
const app = express();

require('dotenv').config() ;
require('./config/passport')(passport);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(bodyParser.json());
app.use(expressValidator());

app.use(passport.initialize());
app.use(passport.session());

mongoose.connect( process.env.MONGO_DB_URL, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(()=> console.log("MongoDB successfully connected"))
    .catch(err => console.log(err));


app.use('/api', apiRouter);

app.get('/test', (req,res) => {
    res.send('test')
});


app.listen(port,() => console.log(`Listen on : ${port}`));




// module.exports = app;
